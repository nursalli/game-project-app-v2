import {calculateSuit} from '../suitScript';

test('user rock vs com rock expected Draw', () => {
  expect(calculateSuit('rock', 'rock')).toMatch('draw');
});
test('user paper vs com rock expected Win', () => {
  expect(calculateSuit('paper', 'rock')).toMatch('win');
});
test('user scissor vs com rock expected Lose', () => {
  expect(calculateSuit('scissor', 'rock')).toMatch('lose');
});
